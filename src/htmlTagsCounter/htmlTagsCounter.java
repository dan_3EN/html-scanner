package htmlTagsCounter;
/**
 * @author HOD15097490
 */
import java.util.Scanner;
import p1utils.FileScanner;

public class htmlTagsCounter 
{
    /**
     *this program enables you to have a file of HTML tags 
     * and then sort though another HTML file to find all
     * the tags, to then count them and then display the 
     * lines that the specific tags are on
     */

    public static void main(String[] args) {
        
       int userChoice = 0;
       Scanner kb = new Scanner(System.in);
       String[] headList = null;     
       String[] mmList = null;
       String[] hlList = null;
       FileScanner input = null;
     
       
       do{
           do{ // start of validation do while loop
                System.out.println("1. load SEO tags definitions qwerty");//these are your menu options
                System.out.println("2. display the SEO tags definitions");
                System.out.println("3. load and analyse the HTML file");
                System.out.println("4. quit");
                System.out.println("");
                System.out.print("Option: ");
                
                try{
                userChoice = kb.nextInt(); //this is where you inout your menu choice
                kb.nextLine();
                }
                catch(Exception ex){
                    System.out.println("ERROR: Values 1 to 4 only");//validation for if an incorrect option is input
                    System.out.println(ex);
                    kb.nextLine();
                }
                
           }while(userChoice < 1 || userChoice > 4);//end of the do while loop for validation
           
           switch (userChoice){
            case 1: //this case is what reads in the html tags from the tags.txt file
                System.out.println("Loading SEO tags");
                System.out.println("Enter the path to the SEO tags");
                    String tagsFilePath = kb.next(); //this is where you type in the file name
                    
                    
                try{
                    input = new FileScanner(tagsFilePath); //this makes the input to be the contents of the tags file
                    
                    Integer currentPosition = 0;
                    Integer arraySize = 0;
                    Boolean finish = false;
                    
                    do { //this do while loop assigns the sizes of the arrays and fills them with the tags
                        if(currentPosition == 0) { //this part of the if statement is what gets the array size
                            if(null != headList && null != mmList && null != hlList) {
                                finish = true;
                            } else {
                                arraySize = input.nextInt();
                                currentPosition = arraySize;
                                if(null == headList) {
                                    headList = new String[currentPosition];
                                } else if (null == mmList) {
                                    mmList = new String[currentPosition];
                                } else if (null == hlList) {
                                    hlList = new String[currentPosition];
                                } 
                            }
                            input.nextLine();
                        } else { //this part of the if statement is what fills the arrays with data
                          if(null != hlList) {
                                hlList[arraySize - currentPosition] = input.nextLine();
                            } else if (null != mmList) {
                                mmList[arraySize - currentPosition] = input.nextLine();
                            } else if (null != headList) {
                                headList[arraySize - currentPosition] = input.nextLine();
                            }
                          currentPosition--;
                        }
                    } while(finish == false);
                    
                }catch(Exception ex){ // this catched the error if the code fails and prints out the error
                   System.out.println(ex);
                }
               
                break; // end of case 1
                
            case 2: // this displays the tags in their arrays
                if(headList == null){ // this makes sure that option 1 was run before option 2
                    System.out.println("Option 1 has not run, plaese select option 1 before any other");
                } else {
                System.out.println("Displaying the SEO tags"); 
                
                 for(int count = 0; count < headList.length; count++) // these for loops are for displaying the data in the arrays
                 {
                   System.out.println(headList[count]);
                 }  
                 for(int count = 0; count < mmList.length; count++)     
                 {
                   System.out.println( mmList[count]);
                 }
                 for(int count = 0; count < hlList.length; count++)     
                 {
                   System.out.println( hlList[count]);
                 }
                }

                break; // end of case 2
            case 3: // this case is what reads the html file and sorts through the tags and counts and finds their line number 
                
                if(headList == null){ // this makes sure that option 1 was run before option 3
                    System.out.println("Option 1 has not run, plaese select option 1 before any other");
                } else {
                String htmlFilePath = "";
                System.out.println("Analysing");
                
                 try { //this is for validation to make sure the html file name is correct
                    System.out.println("Enter the path to the HTML file");
                    htmlFilePath = kb.nextLine();  // this line is where you enter in the filename of the html file
                    FileScanner htmlScanner = new FileScanner(htmlFilePath);
                    String line = htmlScanner.nextLine(); // this line variable is used to get and mark down the line number for the html file
                    System.out.println("\n" + "HTML FILE LISTING");
                    System.out.println("=================" + "\n");
                    
                    
                    while(line != null){
                        if(htmlScanner.getLineNumber()<10){
                            System.out.print("0" + htmlScanner.getLineNumber());//adds a 0 to the start of the line number if it is only 1 digit
                        } else{
                            System.out.print(htmlScanner.getLineNumber()); //prints out the line number
                        }
                        System.out.println("" + line);
                        line = htmlScanner.nextLine(); //prints out the contents of the line
                    }
                    System.out.println("\n");
                       
            } catch(Exception ex){ // this is the catch for if the file name is wrong
            System.out.println("Error - Enter valid file name\n" + ex);
            }
             
             int[] headersTagCount = new int[headList.length]; //these make the integer arrays for the count the correct length for the tags
             int[] multimediaTagCount = new int [mmList.length];
             int[] hyperlinkTagCount = new int [hlList.length];
             
             String[] newHeaderTagFoundLine = new String[headList.length];  //these are used for making sure the line number array is the right length
             String[] newMultimediaTagFoundLine = new String[mmList.length];
             String[] newHyperlinkTagFoundLine = new String[hlList.length];
             
             try{
                 FileScanner tagsCountScanner = new FileScanner(htmlFilePath); // this is to read in the html file
                 String tagCountWord = tagsCountScanner.next();
                 
                 while(tagCountWord != null){ //this while loop is to keep this process running until there are no more lines of the html file left
                     
                     for(int hTagsCount = 0; hTagsCount < headList.length; hTagsCount ++){ // this is the for loop to create the headings tags count and their line numbers
                         if(tagCountWord.contains(headList[hTagsCount])){
                             headersTagCount[hTagsCount]++;
                             if(newHeaderTagFoundLine[hTagsCount] == null){
                             newHeaderTagFoundLine[hTagsCount] = "" + tagsCountScanner.getLineNumber() + ", ";
                             } else{
                                newHeaderTagFoundLine[hTagsCount] += "" + tagsCountScanner.getLineNumber() + ", ";
                             }
                         }
                     }
                     for(int mmTagsCount = 0; mmTagsCount < mmList.length; mmTagsCount ++){ // this is the for loop to create the multimedia tags count and their line numbers
                         if(tagCountWord.contains(mmList[mmTagsCount])){
                             multimediaTagCount[mmTagsCount]++;
                             if(newMultimediaTagFoundLine[mmTagsCount] == null){
                             newMultimediaTagFoundLine[mmTagsCount] = "" + tagsCountScanner.getLineNumber() + ", ";
                             } else{
                                 newMultimediaTagFoundLine[mmTagsCount] += "" + tagsCountScanner.getLineNumber() + ", ";
                             }
                         }
                     }
                     for(int hlTagsCount = 0; hlTagsCount < hlList.length; hlTagsCount ++){// this is the for loop to create the hyperlink tags count and their line numbers
                         if(tagCountWord.contains(hlList[hlTagsCount])){
                             hyperlinkTagCount[hlTagsCount]++;
                             if(newHyperlinkTagFoundLine[hlTagsCount] == null){
                             newHyperlinkTagFoundLine[hlTagsCount] = "" + tagsCountScanner.getLineNumber() + ", ";
                             } else{
                             newHyperlinkTagFoundLine[hlTagsCount] += "" + tagsCountScanner.getLineNumber() + ", ";
                             }
                         }
                     }
                     tagCountWord = tagsCountScanner.next();
                 } // end of while loop
                 
                 
                 System.out.println("Headings Tags:" + "\n"); // this prints out the the titles for the heading tags
                 System.out.println("Tag" + "\t" + "Count" + "\t" + "On Lines");
                 System.out.println("===" + "\t" + "=====" + "\t" + "===============" +"\n");
                 
                 for( int resultsCount = 0; resultsCount < headList.length; resultsCount++){ //this for loop is what prints out the tags, count, and which lines they are on
                     if(newHeaderTagFoundLine[resultsCount]== null){
                         System.out.print("");
                     } else{
                     System.out.println(headList[resultsCount] + "\t" + headersTagCount[resultsCount] + "\t" + newHeaderTagFoundLine[resultsCount] + "\b\b");
                     }
                 }
                 
                 System.out.print("\n"); // this prints out the the titles for the multimedia tags
                 System.out.println("Multimedia Tags:" + "\n");
                 System.out.println("Tag" + "\t" + "Count" + "\t" + "On Lines");
                 System.out.println("===" + "\t" + "=====" + "\t" + "===============" +"\n");

                 for( int resultsCount = 0; resultsCount < mmList.length; resultsCount++){ //this for loop is what prints out the tags, count, and which lines they are on
                     if(mmList[resultsCount].contains("<")){
                         if(newMultimediaTagFoundLine[resultsCount] == null){
                            System.out.print(""); 
                         }else{
                         System.out.println(mmList[resultsCount] + "\t" + multimediaTagCount[resultsCount] + "\t" + newMultimediaTagFoundLine[resultsCount] + "\b\b");
                         }
                     }
                 }
                 
                 System.out.print("\n"); // this prints out the the titles for the hyperlink tags
                 System.out.println("Hyperlink and Head Tags:" + "\n");
                 System.out.println("Tag" + "\t" + "Count" + "\t" + "On Lines");
                 System.out.println("===" + "\t" + "=====" + "\t" + "===============" +"\n");
                 
                 for( int resultsCount = 0; resultsCount < hlList.length; resultsCount++){ //this for loop is what prints out the tags, count, and which lines they are on
                     if(hlList[resultsCount].contains("<")){
                         if(newHyperlinkTagFoundLine[resultsCount] == null){
                             System.out.print("");
                         } else{
                            System.out.println(hlList[resultsCount] + "\t" + hyperlinkTagCount[resultsCount] + "\t" + newHyperlinkTagFoundLine[resultsCount] + "\b\b");
                         }
                    }
                 }
                 
                 System.out.println("\n" + "END OF ANALYSIS" + "\n");
                 

                    }catch(Exception ex){
                        System.out.println("ERROR" + ex + "\n" + "Error - didnt create a filescanner");
                    }
               } // end of validation if
                break;
            case 4:
                System.out.println("Goodbye");
                break;
            default: //this will trigger if the number entered at the start is not between 1 and 4
                System.out.println("invalid menu option, please select of of the valid options asked of you");
                break;
        }
           
       }while(userChoice != 4); // the code will keep repeating until the number 4 is triggered
    
       System.exit(0); // the end of the code 
    } // end of main meathod
   
} // end of class